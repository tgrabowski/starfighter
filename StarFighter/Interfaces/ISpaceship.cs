﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace StarFighter
{
    interface ISpaceship
    {
        string Typ { get; set; }
        string Nazwa { get; set; }
        Vector2 Pozycja { get; set; }
        Texture2D Tekstura { get; set; }
        int Tarcze { get; set; }
        int Struktura { get; set; }
        List<Strzal> Strzaly { get; set; }

        void ruch();
        void strzel();
        void zniszczSie();

    }
}
