﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace StarFighter
{
    class AlienShip : ISpaceship
    {
        #region properties
        
        private string typ;
        private string nazwa;
        private Vector2 pozycja;
        private Texture2D tekstura;
        private int tarcze;
        private int struktura;

        public string Typ
        {
            get
            {
                return typ;
            }
            set
            {
                
            }
        }        
        public string Nazwa
        {
            get
            {
                return nazwa;
            }
            set
            {
                
            }
        }        
        public Vector2 Pozycja
        {
            get
            {
                return pozycja;
            }
            set
            {
                
            }
        }
        public Texture2D Tekstura
        {
            get
            {
                return tekstura;
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        public int Tarcze
        {
            get
            {
                return tarcze;
            }
            set
            {
                tarcze = value;
            }
        }
        public int Struktura
        {
            get
            {
                return struktura;
            }
            set
            {
                struktura = value;
            }
        }
        public List<Strzal> Strzaly
        {
            get
            {
                return Strzaly;
            }
            set
            {
                
            }
        }

        #endregion

        #region methods

        public void ruch()
        {
            throw new NotImplementedException();
        }

        public void strzel()
        {
            throw new NotImplementedException();
        }

        public void zniszczSie()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
